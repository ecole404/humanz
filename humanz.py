class humanz():
    arguments = {
        'name': str,
        'sex': str,
        'age': int
    }
    @classmethod
    def Form(cls, data: dict, complete: bool):
        if(type(data) == dict):
            if((complete and len(data) == len(cls.arguments)) or (not complete and len(data) > 0)):
                for key in data:
                    if(key in cls.arguments):
                        if(type(data[key]) == cls.arguments[key]):
                            pass
                        else:
                            return False
                    else:
                        return False
                return True
        return False

    def __init__(self, data):
        self.id = data[0]
        self.name = data[1]
        self.genre = data[2]
        self.age = data[3]
    
    def serialize(self):
        return {
            "id": self.id,
            "name": self.name,
            "genre": self.genre,
            "age": self.age
        }
    
