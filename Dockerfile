#crée un conteneur à partire de l'image de python 3
FROM python:3

#lance la commande dans le conteneur
RUN pip install --no-cache-dir flask
RUN pip install --no-cache-dir mysql-connector-python

#set working directory
WORKDIR /app

#copy project files
COPY . .

# expose sur le port 500 mais sur son propre localhost
EXPOSE 5000

# add environment variables
# ENV FLASK_ENV=development

# CMD ["python", "/app/main.py"]
# execute cette fonction
CMD python /app/main.py
