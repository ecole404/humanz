from flask import Flask, request, jsonify, g  # pip install flask
from humanz import humanz
import os
import mysql.connector

app = Flask(__name__)

def get_db():
    if not 'db' in g:
        g.db = mysql.connector.connect(
            host="db",
            port="3306",
            user=os.environ['DB_USER'],
            passwd=os.environ['DB_PASSWORD'],
            database = os.environ['DB_NAME']
        )
    return g.db

def get_cursor():
    return get_db().cursor()

@app.after_request
def cleanup(response):
    if 'db' in g:
        g.db.close()
        del g.db
    return response

def getHumans():
    mycursor = get_cursor()
    mycursor.execute("""SELECT * FROM Human""")
    rows = mycursor.fetchall()
    mycursor.close()
    data = [humanz(x) for x in rows]
    return jsonify([d.serialize() for d in data])


def getHuman(human_id: int) -> humanz:
    cursor = get_cursor()
    cursor.execute(""" SELECT * FROM Human WHERE id = %(human_id)s""",{
            'human_id': human_id
        })
    rawResult = cursor.fetchone()
    result = "No human" if rawResult == None else humanz(rawResult).serialize()
    return jsonify(result)


def deleteHuman(human_id):
    connection = get_db()
    cursor = connection.cursor()
    cursor.execute("""DELETE FROM Human WHERE id = %s""",(human_id))
    connection.commit()
    return jsonify()

def postHuman(data):
    if(humanz.Form(data, True)):
        connection = get_db()
        cursor = connection.cursor()

        mySql_insert_query = "INSERT INTO `Human` ("
        i = 0
        infos = []

        for key in data:
            mySql_insert_query += "`" + key + "`"
            infos.append(data[key])
            if(i < len(data)-1):
                mySql_insert_query += ","
            i += 1

        mySql_insert_query += ") VALUES (%s, %s, %s)"

        cursor.execute(mySql_insert_query, tuple(infos))
        connection.commit()
        lastId = cursor.lastrowid
        cleanup({})
        return getHuman(lastId)
    return jsonify("Incorect values in form")


def modifyHuman(human_id, data):
    if(humanz.Form(data, False)):
        connection = get_db()
        cursor = connection.cursor()

        mySql_insert_query = "UPDATE `Human` SET"

        i = 0
        infos = []

        for key in data:
            mySql_insert_query += " `"+key+"`= %s "
            infos.append(data[key])
            if(i < len(data)-1):
                mySql_insert_query += ","
            i += 1

        mySql_insert_query += "WHERE id = %s"
        infos.append(human_id)

        cursor.execute(mySql_insert_query, tuple(infos))
        connection.commit()

        cleanup({})
        return getHuman(human_id)
    return jsonify("Incorect values in form")



@app.route("/api/v1/humanz", methods=['GET', 'POST'])
def getAll():
    if request.method == "GET":
        return getHumans()

    if request.method == "POST":
        return postHuman(request.json)


@app.route("/api/v1/humanz/<int:human_id>", methods=['GET', 'DELETE', 'PUT'])
def getId(human_id):
    if request.method == "GET":
        return getHuman(human_id)

    if request.method == "PUT":
        return modifyHuman(human_id, request.json)
        # return modifyHuman(human_id, request.json)
    if request.method == "DELETE":
        return deleteHuman(human_id)

if __name__ == "__main__":
    # pour qu'il diffuse sur tout
    app.run(host='0.0.0.0')
