-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:8889
-- Généré le :  mer. 20 nov. 2019 à 15:22
-- Version du serveur :  5.7.26
-- Version de PHP :  7.3.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Base de données :  `pythonTest`
--

-- --------------------------------------------------------

--
-- Structure de la table `Human`
--

CREATE TABLE `Human` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `sex` varchar(255) NOT NULL,
  `age` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `Human`
--

INSERT INTO `Human` (`id`, `name`, `sex`,`age`) VALUES
(1, 'John', 'Hommme',10),
(2, 'LeBg', 'Hommme',10),
(3, 'OuiOui', 'Hommme',10),
(4, 'Dora', 'Femme',10),
(5, 'Baptiste', 'Hommme',10),
(6, 'BuzzL\'eclaire', 'Hommme',10),
(7, 'Arya', 'Femme',10),
(8, 'Sansa', 'Femme',10),
(9, 'Catelyne', 'Femme',10),
(10, 'Pierre', 'Homme',10),
(11, 'Lucas', 'Homme',10);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `Human`
--
ALTER TABLE `Human`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `Human`
--
ALTER TABLE `Human`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
